class Map {
  xml2js
  fs
  moment
  requiredArgs = [
    'output'
  ]

  constructor(xml2js, fs, moment) {
    this.xml2js = xml2js;
    this.fs = fs;
    this.moment = moment;
  }

   getTestCases(testRun) {
    var cases = [];
    testRun["test-suite"].forEach(suite => {
      cases = cases.concat(this.recurseSuites(suite));
    });
    return cases;
  }

  recurseSuites(suite) {
    if(!suite["test-suite"]) {
      return suite["test-case"];
    }
    var cases = []
    suite["test-suite"].forEach(suite => {
      cases = cases.concat(this.recurseSuites(suite));
    });
    return cases;
  }

  async handleCommand(args) {
    if (args['_'].length < 2) {
      throw new Error(`You must provide a file to map! See obsidian-nunit-mapper map --help.`)
    }

    for (let i = 0; i < this.requiredArgs.length; i++) {
      if(!args[this.requiredArgs[i]]) {
        throw new Error(`Required argument ${this.requiredArgs[i]} not supplied. See obsidian-nunit-mapper map --help.`)
      }
    }

    try {
      const inputFilename = args['_'][1];
      const inputFileContent = await this.fs.readFile(inputFilename);
      const result = await this.xml2js.parseStringPromise(inputFileContent);
      const testRunJson = result["test-run"];
      let testRun = {
        startTimeUtc: testRunJson['$']["start-time"],
        endTimeUtc: testRunJson['$']["end-time"]
      };
      const testCases =  this.getTestCases(testRunJson);

      let executions = [];
      testCases.filter(c => c != null).forEach(testCase => {
        var execution = {
          succeeded: testCase['$'].result === 'Passed',
          testName: testCase['$'].name,
          duration:  testCase['$'].time * 1000
        };
        if(!execution.succeeded && testCase.failure) {
          const failure = testCase.failure[0];
          execution.message = failure.message ? failure.message[0].replace('\n', ',').replace(/\s+/g,' ').trim() : '';
          execution.stackTrace = failure['stack-trace'] ? failure['stack-trace'][0].replace('\n', ',').replace(/\s+/g,' ').trim() : '';
        }
        
        executions.push(execution);
      });

      testRun.success = executions.every(exec => exec.succeeded);
      testRun.passes = executions.filter(exec => exec.succeeded).length;
      testRun.failures = executions.filter(exec => !exec.succeeded).length;
      testRun.executions = executions;

      await this.fs.writeFile(args.output, JSON.stringify(testRun));
    } catch(err) {
      if (args.verbose) {
        console.error(err);
      }

      throw new Error(`Encountered an error while reading and mapping your input NUnit.`);
    }
  }
}

module.exports = Map;